module.exports = {
    defaultCommandTimeout: 5000,
    requestTimeout: 10000, 
    screenshotsFolder: 'cypress/screenshots', 
    videosFolder:      'cypress/videos', 
    video: true, 
    videosOnFailureOnly: true, 
    chromeWebSecurity: false,
    env: {
        baseUrl: 'https://www.saucedemo.com/'
      }
  };
  