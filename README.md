# Automated UI and API Tests

This project contains automated UI and API tests using Cypress with JavaScript. The tests include:

- UI tests: Verify the functionality of the Sauce Demo website.
- API tests: Retrieve and verify objects from a public API.

## Challenge Description

The challenge consists of two parts: UI tests and API tests.

**UI Tests:**
1. Go to the [Sauce Demo website](https://www.saucedemo.com/).
2. Log in to the site using the provided credentials.
3. Verify that the items are sorted by Name (A -> Z).
4. Change the sorting to Name (Z -> A).
5. Verify that the items are sorted correctly.

**API Tests:**
1. Call the [Public APIs](https://api.publicapis.org/entries) API.
2. Read the response and find all objects with the property "Category: Authentication & Authorization".
3. Compare, count, and verify the number of objects where the property above appears.
4. Print the found objects to the console.

## Requirements

 - [Node.js]( )
 - [Cypress](https://www.cypress.io/)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/marinachibi/test-symphony-solutions-main
```
2. Navigate to the project directory.
3. Install the dependencies:

```bash
npm install
   ```

## Running the Tests

### UI Tests
To run the UI tests, use one of the following commands:

- To open the Cypress Test Runner:

```bash
npm run cy:open:e2e
```
- To run the tests in headless mode:

```bash
npm run cy:run:e2e
```
### API Tests
To run the API tests, use one of the following commands:

- To open the Cypress Test Runner:

```bash
npm run cy:open:API
```
- To run the tests in headless mode:

```bash
npm run cy:run:API
```
## Test Reports
The test reports can be found in the Cypress results folder after running the tests. The reports include detailed information about the test runs, including any failures and screenshots.

## CI/CD Pipeline

This project includes a CI/CD pipeline set up in GitLab. The pipeline runs the automated tests on each commit. 
- [Pipelines](https://gitlab.com/marinachibi/test-symphony-solutions/-/pipelines) 

## License
This project is licensed under the MIT License.
