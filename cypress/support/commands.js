// cypress/support/commands.js

Cypress.Commands.add('login', (username) => {
  cy.fixture('credentials').then((data) => {
    const password = data.users[username];
    cy.get('#user-name').type(username);
    cy.get('#password').type(password);
    cy.get('#login-button').click();
    cy.url().should('include', '/inventory.html');
  });
});