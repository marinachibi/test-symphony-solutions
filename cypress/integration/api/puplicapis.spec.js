/// <reference types="cypress" />

describe('API Tests', () => {
  it('should retrieve and verify objects with Category: Authentication & Authorization', () => {
    cy.request('https://api.publicapis.org/entries').then((response) => {
      const objects = response.body.entries;
      const authObjects = objects.filter((obj) =>
        obj.Category.includes('Authentication & Authorization')
      );

      const objectCount = authObjects.length;
      cy.log(`Number of objects with Category 'Authentication & Authorization': ${objectCount}`);

      cy.log('Objects:');
      authObjects.forEach((obj) => {
        cy.log(JSON.stringify(obj));
      });

      expect(objectCount).to.be.above(0);
    });
  });
});
