/// <reference types="cypress" />

describe('UI Tests', () => {
  beforeEach(() => {
    cy.visit('https://www.saucedemo.com/');
    cy.login('standard_user');
  });

  it('verify items are sorted by Name (A -> Z)', () => {
    cy.get('.inventory_item_name')
      .invoke('text')
      .then((itemText) => {
        const itemNames = itemText
          .split('\n')
          .filter((text) => text.trim().length > 0);
        const sortedItemNames = [...itemNames].sort();
        expect(itemNames[0]).to.deep.equal(sortedItemNames[0]);
      });
  });

  it('verify items are sorted by Name (Z -> A)', () => {
    cy.get('.product_sort_container').select('za');
    cy.get('.inventory_item_name')
      .invoke('text')
      .then((itemText) => {
        const itemNames = itemText
          .split('\n')
          .filter((text) => text.trim().length > 0);
        const sortedItemNames = [...itemNames].sort().reverse();
        expect(itemNames[0]).to.deep.equal(sortedItemNames[0]);
      });
  });
});
